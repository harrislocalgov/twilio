exports.handler = function(context, event, callback) {

const request = require('request');
var data = "";
if(event.getFunction == "SendIVRPaymentAllUtility")
{
  data = JSON.stringify({"account":event.account, "password":event.password,
 "convenienceFee":event.convenienceFee, "confirmation":event.confirmation})
}

else if(event.getFunction == "SendIVRPaymentSingleProperty")
{
  data = JSON.stringify({"account":event.account, "password":event.password, 
  "convenienceFee":event.convenienceFee, "confirmation":event.confirmation,
  "singlePropertyID":event.singlePropertyID, "amount":event.amount})
  }
else if(event.getFunction == "SendIVRPaymentTax")
{
  data = JSON.stringify({"taxYear":event.taxYear, "billNumber":event.billNumber, "password":event.password, 
  "convenienceFee":event.convenienceFee, "confirmation":event.confirmation, "amount":event.amount})
  }
  
  
var options = {
  'method': 'POST',
  'url': event.mainURL + '/api/Payment/' + event.getFunction,
  'headers': {
    'Content-Type': 'application/json'
  },
  body: data

};

request(options, function (error, response) {
  if (error) callback(error);
  const results = JSON.parse(response.body);
  callback(null, results);
});

  
}